const user 			= require('./account');
const vote 			= require('./vote');

module.exports ={
	user,
	vote
};