const Vote			= require('../Model/vote');
const async         = require("async");
const User          = require('../Model/user');

module.exports = {
    postVote(req,res){
        
        async.waterfall([
            (done)=>{
                let currentUser;
                User.findOne({email:req.body['currentUser']},(err,data)=>{
                    currentUser = data['_id'];
                    done(err,currentUser);
                });
            },
            (currentUser, done) => {
                let voteUser;
                User.findOne({email:req.body['voteUser']},(err,data)=>{
                    
                    voteUser = data['_id'];
                    done(err,currentUser,voteUser);
                });
            },
            (currentUser, voteUser, done)=>{
                let voteData = new Vote();
                voteData.userVotingId = voteUser;
                voteData.UserchoosingId = currentUser;
                voteData.save();
                done(null,currentUser);
                // done('done');
                // return res.status(200).send({
                //     success:true,
                //     message:'Success'
                // })
            },
            (currentUser, done) => {
                User.findOneAndUpdate({_id:currentUser},{status:1},{useFindAndModify: false},(err, data)=>{
                    done(null,'done');
                    return res.status(200).send({
                        success: true,
                        message: 'Success',
                        user:data
                    })
                })
            }
        ], (err) => {
             if (err)
                 res.status(400).send({
                     success: false,
                     message: 'Failed'
                 });
        });
    },
    getAllVote(req,res){
                Vote.aggregate([
                 {
                // $group: {_id:"$userVotingId",
                // count:{$sum:1}}
                $lookup: {
                    from: "users",
                    localField: "userVotingId",
                    foreignField: "_id",
                    as: "user"
                }
            },
            {
                $group: {
                    _id: '$user',
                    count: {
                        $sum: 1
                    },

                }
            },
            {
                $sort: {
                    count: -1
                }
            }
            ]).exec((err,data)=>{
                if (err)
                    res.status(400).send({
                        success: false,
                        message: err
                    })

                return res.status(200).send({
                    success:true,
                    vote:data
                })
            })
    },
    getDetailVote(req,res){
        Vote.find({userVotingId:req.params.id}).populate({path:'UserchoosingId',select:['name','email']}).exec((err,data)=>{
            if (err)
                return res.status(400).send({
                    success:false,
                    message:'Failed'
                })

            return res.status(200).send({
                success:true,
                detail:data
            })
        })
        // SalesApproval.find({}).populate('userVotingId').
        //         exec(function (err, dataAdmin) {
        //             done(err,admin,user,dataHeader,dataSummary,dataARList,dataSOList,dataCheckList,dataAdmin);  
        //             if (err) return handleError(err);
        //         });
    }


};