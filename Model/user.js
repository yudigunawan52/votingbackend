const mongoose    = require('mongoose');
const Schema      = mongoose.Schema;

const UserSchema  = new Schema({
  email: { type: String, unique: true, lowercase: true },
  name: String,
  password: String,
  status: { type: Number, required: true, maxlength: 1, default: 0 } //0 belum vote 1 sudah vote
});

module.exports = mongoose.model('User', UserSchema);