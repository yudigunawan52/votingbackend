const mongoose    = require('mongoose');
const Schema      = mongoose.Schema;

const VoteSchema = new Schema({
    userVotingId : { type:Schema.Types.ObjectId, ref:'User'},
    Date: { type: Date, default: Date.now },
    UserchoosingId:{ type:Schema.Types.ObjectId, ref:'User'}
});

module.exports = mongoose.model('Vote', VoteSchema);