#  Cara Menjalankan Backend

*  note:( jika ingin menjalankannya dilocal pergi ke folder `Config` didalam nya ada folder `Mongodb` dan didalam itu ada `mongodb.js` tukar database nya dengan
        yang telah dicomment)
*  Untuk menjalankan backend nya pertama kita harus menjalankan command `npm install` di terminal kita untuk 
   menginstall library yang telah didefenisikan di `package.json`
*  Setelah itu buatlah database dimongodb local kita yang mana hostnya yaitu `localhost` dan port `27017`
   dan buatlah database nya dengan nama `Voting` atau bisa dengan merestore database mongo nya dengan 
   cara mengcopy folder `dump` dan dipastekan ke `bin` Mongodb lalu kita bisa menjalankan command `mongorestore` 
   agar merestore database mongodb tersebut
*  lalu kita bisa menjalankan backendnya dengan cara menjalankan command `nodemon` atau `node index.js` dan
   backend berjalan