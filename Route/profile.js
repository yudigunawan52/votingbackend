const router = require('express').Router();
const accountController = require('../Controller').user;
router.get('/', accountController.getProfile);
router.get('/all-user', accountController.getAllUser);

module.exports = router;