const router = require('express').Router();
const voteController = require('../Controller').vote;

router.post('/post-vote', voteController.postVote);
router.get('/all-vote', voteController.getAllVote);
router.get('/detail-vote/:id', voteController.getDetailVote);

module.exports = router;