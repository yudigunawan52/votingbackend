const chai          = require('chai');
const chaiHttp      = require('chai-http');
const server        = require('../app');
const should        = chai.should();
chai.use(chaiHttp);

describe('Profile', () => {
    let user = {
        email: "yudigunawan52@gmail.com",
        password: "111111"
    }
    
    describe('/POST Login', () => {
        it('Post Login Success', (done) => {
            chai.request(server)
            .post('/api-login')
            .send(user)
            .end((err, res) => {
                token=res.body['token'];
                res.statusCode.should.equal(200);
                done();
            });
        });
    });

    describe('/Get Profile', () => {
        it('Get Profile Success', (done) => {
            chai.request(server)
            .get('/api-profile')
            .set('Authorization',token)
            .end((err, res) => {
                res.statusCode.should.equal(200);
                done();
            });
        });
    });

    describe('/Get All User', () => {
        it('Get All User Success', (done) => {
            chai.request(server)
                .get('/api-profile/all-user')
                .set('Authorization', token)
                .end((err, res) => {
                    res.statusCode.should.equal(200);
                    done();
                });
        });
    });
});