const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const should = chai.should();
chai.use(chaiHttp);

describe('Vote', () => {
    let user = {
        email: "yudigunawan52@gmail.com",
        password: "111111"
    }

    let vote={
        id : "5e3506d0b75a55165c1bdb12"
    }

    describe('/POST Login', () => {
        it('Post Login Success', (done) => {
            chai.request(server)
                .post('/api-login')
                .send(user)
                .end((err, res) => {
                    token = res.body['token'];
                    res.statusCode.should.equal(200);
                    done();
                });
        });
    });

    describe('/Get All Vote', () => {
        it('Get All Vote Success', (done) => {
            chai.request(server)
                .get('/api-vote/all-vote')
                .set('Authorization', token)
                .end((err, res) => {
                    res.statusCode.should.equal(200);
                    done();
                });
        });
    });

    describe('/Get Detail Vote', () => {
        it('Get Detail Vote Success', (done) => {
            chai.request(server)
                .get('/api-vote/detail-vote/'+vote['id'])
                .set('Authorization', token)
                .end((err, res) => {
                    res.statusCode.should.equal(200);
                    done();
                });
        });
    });
});